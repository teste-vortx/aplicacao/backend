module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'todolist_on_herbs_db',
      user: 'postgres',
      password: 'postgres',
      host: 'a6d37ea6e84514686b27f72bf05c11b5-829575628.us-east-1.elb.amazonaws.com',
      port: 5432
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './src/infra/db/migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: './src/infra/db/seeds/dev'
    }
  },

  staging: {},

  production: {}

}
